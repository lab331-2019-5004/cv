import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from './service/student-service';
import { StudentDataImplService } from './service/student-data-impl.service';
import { HttpClientModule } from '@angular/common/http';
import { StudentsFileImplService } from './service/students-file-impl.service';
import { NewStudentsImplService } from './service/new-students-impl.service';
@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    {provide: StudentService, useClass: NewStudentsImplService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
