import { TestBed } from '@angular/core/testing';

import { NewStudentsImplService } from './new-students-impl.service';

describe('NewStudentsImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewStudentsImplService = TestBed.get(NewStudentsImplService);
    expect(service).toBeTruthy();
  });
});
